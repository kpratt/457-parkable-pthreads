#include<iostream>
#include<memory>
#include<executor_service.hpp>
#include<thread_pool.hpp>
#include<callable.hpp>
#include<memory>

using namespace std;

class fibinacci : public callable {

    int n;

public:
    fibinacci(int n):
        n(n)
    {}
    virtual ~fibinacci() = default;

    virtual void* call() {
        size_t* ret = new size_t;
        size_t x, y;
        size_t i;
        if(n == 0){
            *ret = 0;
        } else if(n == 1) {
            *ret = 1;
        } else {
            x=0;
            y=1;
            for(i=2; i<n; i++){
                *ret = x + y;
                x = y;
                y = *ret;
            }
        }
        return ret;
    }
};

class fixed_number : public callable {

public:
    fixed_number(){}
    virtual ~fixed_number() = default;

    virtual void* call() {
        return new size_t(5);
    }
};

void test_promise(){
    fixed_number* ptr = new fixed_number();
    shared_ptr<callable> task(ptr);
    shared_ptr<future> f(new future());
    auto p =
        shared_ptr<promise>(
          new promise(task, f)
        );
    p->run();
    unique_ptr<size_t> result(f->get<size_t>(1));
    if(*result == 5) {
        cout << "success" << endl;
    } else {
        cout << "failure" << endl;
    }
}

int main() {

    cout << "makeing thread pool" << endl;
    executor_service* exec =
        new thread_pool<10, 1>(1, 0);

    cout << "createing future" << endl;
    shared_ptr<future> future =
        exec->submit(
            shared_ptr<callable>(new fibinacci(7))
        );

    cout << "awaiting get" << endl;
    unique_ptr<size_t> result(future->get<size_t>(100));

    cout << "value = " << *result << endl;
}
