#pragma once

#include<algorithm>
#include <thread>

using namespace std;
#include<iostream>

#include <chrono>
using ms =
    std::chrono::duration<size_t, std::ratio<1, 1000>>;

class future {

friend class promise;

private:
    void* result;
    bool done;

    void set(void* result) {
       this->result = result; 
       done = true;
    }

    void* getvoid(size_t timeout){
        while(done == false) {
        }
        return result;
    }


public:
    future():
          result(nullptr)
        , done(false)
    {}
    virtual ~future() = default;

    template<typename T>
    T* get(size_t timeout){
        void* ret = getvoid(timeout);
        return (T*)(ret);
    }
};
