#pragma once

#include <mutex>
#include <condition_variable>
#include <unistd.h>
#include <iostream>

using namespace std;

#define MAX_QUEUE_SIZE 4096

// queue structure
template<typename T, size_t capacity>
class blocking_queue {
    size_t head; __attribute__((aligned(64)))
    size_t _size;

    std::array<T, capacity> elements;

    //synchronization variables

    std::mutex m;

public:
    blocking_queue():
        head(0),
        _size(0),
        m()
    {

    }

    ~blocking_queue() = default;

    //function declarations
    void enqueue( T& message ){
      std::unique_lock<std::mutex> lock(m); // go google RAII
        if(_size >= capacity) {
	  // this block of code implements busy waiting on a locked resource
	  // AFTER implimenting dequeue in the first part of your assignment
	  // modify, this block of code (and the block in dequeue) to use conditional variables & baton passing.
	  do {
	    lock.unlock(); // release the lock so dequeue can aquire it
	    usleep(100);   // yeild the processor so some other thread can call dequeueu and make room.
	    lock.lock();   // reaquire the lock befor continuing.
	  } while(_size >= capacity);
        }

        size_t index = (head + _size) % capacity;
        elements[index] = std::move(message);

        _size++;

        // implicit unlock when lock's deconstructor is called
    }

    T dequeue( ) {
    }

    size_t size() {
        return _size;
    }

};
