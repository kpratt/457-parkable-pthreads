#pragma once

#include <algorithm>
#include <pthread.h>
#include <future.hpp>
#include <promise.hpp>
#include <executor_service.hpp>
#include <runnable.hpp>
#include <callable.hpp>
#include <blocking_queue.hpp>
#include <memory>

#include <iostream>
using namespace std;


template<size_t q_capacity>
void* thread_pool_job(void* args){
    return nullptr;
}

template<size_t q_capacity, size_t maxPoolSize>
class thread_pool : public executor_service {

public:

    thread_pool(size_t corePoolSize,
                size_t keep_alive)
    {
    }

    void submit(shared_ptr<runnable> task){
    }

    shared_ptr<future> submit(shared_ptr<callable> task){
      return shared_ptr<future>(new future());
    }
};
