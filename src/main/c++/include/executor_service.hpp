#pragma once

#include<future.hpp>
#include<runnable.hpp>
#include<callable.hpp>
#include<memory>

using namespace std;

class executor_service {

public:
    virtual void submit(shared_ptr<runnable> task) = 0;

    virtual shared_ptr<future> submit(shared_ptr<callable> task) = 0;

};
