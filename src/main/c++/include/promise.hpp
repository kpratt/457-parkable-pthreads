#pragma once
#include <future.hpp>
#include <runnable.hpp>
#include <callable.hpp>
#include <memory>

#include <iostream>
using namespace std;

class promise : public runnable {

private:
    std::shared_ptr<callable> task;
    shared_ptr<future> f;

public:

    promise(shared_ptr<callable> task, shared_ptr<future> f){
        this->task = task;
        this->f = f;
    }
    virtual ~promise() = default;

    void run() {
        cout << "promise executing task." << endl;
        f->set(task->call());
        cout << "promise execution complete." << endl;
    }

};
