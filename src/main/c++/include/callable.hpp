#pragma once
/*
 * A callable is a function-object.
 * It exists to deffer computation in eager languages.
 * One such deffrence is to ask that the computation take place on a seperate thread.
 *
 * The result of the computation must be heap allocated.
 * The pointer type is recovered by the calling code.
 */

class callable {
public:
    virtual void* call() = 0;
};
