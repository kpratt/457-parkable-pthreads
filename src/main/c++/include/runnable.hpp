#pragma once

class runnable {
public:
    virtual void run() = 0;
};
