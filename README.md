For this assignment you are required to implement a blocking queue and a thread pool in C++. This will closely mimic the api you saw in the second (Java Threading) assignment.
You should be able to complete the assignment by modifying 3 files:
    ThreadPool.hpp
    Future.hpp
    BlockingQueue.hpp

Ball park 400 lines of code.

Before you begin, in the terminal:

run "./cmk". If you reviece any compile errors you should alert the TA.
run "./cmk test". This will execute the unit tests under src/test/c++. 
  - That these tests pass is the BARE MINIMUM for a passing grade on the assignment. All tests passing is not a garentee of full marks or correct code.
  - Additionally, 
    - Your blocking queue implementation must correctly perform baton passing and use of conditional variables.
    - Your Thread pool does not need to support additional thread spin up or timeout, only the core pool of threads.
    - Future does not need to support timeout.
    - Hint: Future can be made threadsafe trivially using a BlockingQueue of size 1

Part 1: complete the BlockingQueue implementation. Start by implementing dequeue with busy waiting, enqueue is provided.

Part 2: implement ThreadPool and Future using BlockingQueue & the pthread api. 
     submit should accept a job (runnable / callable)
     when a thread is available the thread dequeues a job and runs to completion
     if a callable is submitted it is wrapped in a promise s.t. the result is given to the future on completion.

Part 3: improve the implementation of the blocking queue to use conditional variables and baton passing instead of busy waiting. As the incorrectness of this can not be reliably verified by unit tests you may find it helpful to write an informal proof. You are NOT required to submit a proof of correctness.

BONUS: implement timeout for future.get & blocking_queue.dequeue

"./cmk test -v"

will run the test code with valgrind. valgrind is a highly useful tool for debugging segmentation faults and memory leaks. It is a requirement of your code that memory leaks do not occure. Therefore the output of valgrind must AT A MINIMUM tell you that 0 bytes where definitely lost, and 0 bytes where indirectly lost. possible loss may be non-zero.

NOTES:
 - ./cmk is a bash script, feel free to open it up and read it.
 - cmake is a widely used tool you can google
 - You should review C++ templates & smart pointers after looking over the code, before starting the assignement.